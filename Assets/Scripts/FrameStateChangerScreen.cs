using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameStateChangerScreen : Screen
{
    public bool frameStateAtInit;
    public bool overlayStateAtInit;
    // Start is called before the first frame update
    public override void Init()
    {
        base.Init();
        UiManager.main.SetFramesState(frameStateAtInit);
        UiManager.main.SetBlackOverlayState(overlayStateAtInit);
    }
}
