using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen : MonoBehaviour
{
    public int id;
    ScreenData data;
    // Start is called before the first frame update
    public void SetData(ScreenData _data)
    {
        data = _data;
        UiManager.main.ShowDataOnBubbles(this, data.textsOnBubble);
    }
    public virtual void Init()
    {
        gameObject.SetActive(true);
        if (data.background != null)
        {
            UiManager.main.SetBackground(data.background);
        }
    }
    public virtual void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
