using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScreenManager : Singleton<ScreenManager>
{
    private List<Screen> screens;
    public List<Screen> orderedScreens;
    int currScreenIndex;
    public void SetScreensData(DialoguesData _data)
    {
        screens = GetComponentsInChildren<Screen>(true).ToList();
        foreach (var screen in screens)
        {
            List<ScreenData> matchedScreensData = _data.screens.Where(d => d.id == screen.id).ToList();
            if (matchedScreensData.Count > 0)
            {
                if (matchedScreensData.Count > 1)
                {
                    Debug.LogError("There are more than one dialogue with the same id, please check the scriptable object and fix it.");
                }
                else
                {
                    screen.SetData(matchedScreensData[0]);
                }
            }
        }
        SetScreensOrder(_data.screens);
        ShowNextScreen();
    }
    public void SetScreensOrder(ScreenData[] screensData)
    {
        foreach (var screenData in screensData)
        {
            Screen matchedScreen = screens.Where(d => d.id == screenData.id).First();
            orderedScreens.Add(matchedScreen);
        }
    }
    public void ShowNextScreen()
    {
        if (currScreenIndex < orderedScreens.Count())
        {
            foreach (var screen in orderedScreens)
            {
                screen.Deactivate();
            }
            orderedScreens[currScreenIndex].Init();
            currScreenIndex++;
        }
    }
}
