using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : Singleton<InputManager>
{
    public Button InteractionObject;
    private void Start()
    {
        InteractionObject.onClick.AddListener(() =>
        {
            ScreenManager.main.ShowNextScreen();
        });
    }
    public void SetInteractionButtonState(bool _state)
    {
        InteractionObject.interactable = _state;
    }
}
