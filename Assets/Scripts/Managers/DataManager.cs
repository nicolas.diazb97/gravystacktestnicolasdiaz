using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    public DialoguesData dialogueData;
    private void Start()
    {
        SetDialoguesData();
    }
    public void SetDialoguesData()
    {
        ScreenManager.main.SetScreensData(dialogueData);
    }
}
