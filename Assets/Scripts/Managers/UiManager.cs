using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using UnityEngine.Events;

public class UiManager : Singleton<UiManager>
{
    public Image backgroundImage;
    public GameObject frames;
    public GameObject blackOverlay;
    UnityEvent onLerpEnd = new UnityEvent();
    // Start is called before the first frame update
    public void LerpColorByImage(Color targetColor, GameObject gameobjectWithImage)
    {
        StartCoroutine(LerpColor(targetColor, 1, gameobjectWithImage.GetComponent<Image>()));
    }
    public UnityEvent GetLerpCallback()
    {
        return onLerpEnd;
    }
    IEnumerator LerpColor(Color endValue, float duration, Image _image)
    {
        float time = 0;
        Color startValue = _image.color;
        while (time < duration)
        {
            _image.color = Color.Lerp(startValue, endValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        _image.color = endValue;
        yield return new WaitForEndOfFrame();
        onLerpEnd.Invoke();
        onLerpEnd.RemoveAllListeners();
    }
    public void ShowDataOnBubbles(Screen _screen, string[] texts)
    {
        List<TextMeshProUGUI> textsOnScreen = _screen.GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
        int currTextIndex = 0;
        foreach (var text in textsOnScreen)
        {
            if (texts.Length > currTextIndex)
            {
                text.text = texts[currTextIndex];
                currTextIndex++;
            }
            else
            {
                Debug.LogError("There is one bubble on screen without information on scriptable object");
            }
        }
    }
    public void SetBackground(Sprite _sprite)
    {
        backgroundImage.sprite = _sprite;
    }
    public void SetFramesState(bool _state)
    {
        frames.SetActive(_state);
    }
    public void SetBlackOverlayState(bool _state)
    {
        blackOverlay.SetActive(_state);
    }
}
