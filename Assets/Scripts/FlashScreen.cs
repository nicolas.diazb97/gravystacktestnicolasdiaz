using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashScreen : Screen
{
    public Color targetColor = new Color(0, 1, 0, 1);
    public Color normalColor = new Color(0, 1, 0, 1);
    public GameObject flashImage;
    bool onFade = false;

    public override void Init()
    {
        base.Init();
        InputManager.main.SetInteractionButtonState(false);
        UiManager.main.LerpColorByImage(targetColor, flashImage);
        onFade = true;
        UiManager.main.GetLerpCallback().AddListener(() =>
        {
            ScreenManager.main.ShowNextScreen();
            InputManager.main.SetInteractionButtonState(true);
        });
    }
    public override void Deactivate()
    {
        base.Deactivate();
        if (onFade)
        {
            UiManager.main.LerpColorByImage(normalColor, flashImage);
            onFade = false;
        }
    }
}
