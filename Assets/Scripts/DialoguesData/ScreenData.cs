using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScreenData
{
    [SerializeField]
    public string screenName;
    [SerializeField]
    [TextArea]
    public string[] textsOnBubble;
    [SerializeField]
    public int id;
    [SerializeField]
    public Sprite background;
}
