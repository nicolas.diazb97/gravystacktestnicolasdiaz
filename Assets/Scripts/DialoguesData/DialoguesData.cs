using System;
using UnityEngine;

[CreateAssetMenu(fileName = "DialoguesData", menuName = "DialoguesData", order = 1)]
public class DialoguesData : ScriptableObject
{
    public ScreenData[] screens;
}
